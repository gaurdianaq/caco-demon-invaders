# Cacodemon Invaders

This was a RAD (Rapid Application Development) Port project for our Game Component Integration class in the GDP Program at Fanshawe College for the 2018-19 school year.
It is a Space Invaders type game clone, where the object of the game is to shoot the enemies at the top of the screen while avoiding enemy fire. You receieve points for each enemy you kill and 
die right away if you are hit by an enemy attack.

## Getting Started

* Clone/Dovwnload this repository to your machine.
* Open the project either using the Unity GUI or by clicking on the .unity file in the Assets/Scenes folder (titlescreen.unity)
* If opening tje project in Unity, to play make sure the titscreen scene is selected and press the Play button at the top of the sceen
* You can also build it as an .exe file by going to File > Build Settings

## Controls
* NOTE: These also appear on the game title scree.
* A and D move the player left and right at the bottom of the screen.
* SPACE fires the players weapon
* Enter starts the game (and returns to the title screen on game completon.

## Built With

* [Unity 2018.2.18](https://unity3d.com/) 

## Our Group Members

* **Evan Tatay-Hinds** <e_tatay-hinds@fanshaweonline.ca>
* **Kristian Kerrigan** <k_kerrigan3@fanshaweonline.ca>
* **Daniel Maclam** <d_maclam@fanshaweonline.ca>
